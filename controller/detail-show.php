<?php

$show_id = 1;
$userEmail;
foreach ($_SESSION as $key => $value) {
	$userEmail = $value->email;
}

$show = new Show();
$showDao = new ShowSqlDao();

$showResult = $showDao->getShow($show_id);

$show->title = $showResult["title"];
$show->description = $showResult["description"];
$show->image_url = $showResult["image_url"];
$show->nb_season = $showResult["nb_season"];


$templateManager = new templateManager();
$templateManager->render("detail-show", 
	[
		'userEmail' => $userEmail,
		'title' => $show->title,
		'description' => $show->description,
		'image_url' => $show->image_url,
		'nb_season' => $show->nb_season
	]);

?>