<?php

$message = "";

if (isset($_POST["email"], $_POST["password"])) {
	$userDao = new UserSqlDao();
	$user = new User();

	$user->email = $_POST["email"];
	$user->password = $_POST["password"];

	$loginResult = $userDao->checkLogin($user);

	if ($loginResult) {
		$_SESSION["user"] = $user;
		$message = "vous êtes connecté";
		header("Location: index.php?ctrl=detail-show");
	}
	else {
		$message = "L'utilisateur n'existe pas.";
	}
}

if (isset($message)) {
	$templateManager = new templateManager();
	$templateManager->render("login", ['message' => $message]);
}

?>