<?php

$userEmail;
foreach ($_SESSION as $key => $value) {
	$userEmail = $value->email;
}

$tags = RetrieveTags();
InjectDataIntoView($userEmail, $tags);
$uploadResult = UploadShowImage();
if ($uploadResult) {
	AddShow();
	AddShowTags();
}


function RetrieveTags() {
	$tagDao = new TagSqlDao();
	$tags = $tagDao->getTags();
	$tagsObject;
	foreach ($tags as $keyTags => $valueTags) {
		$tagObject = new Tag();
		$tagObject->id = $valueTags['id'];
		$tagObject->label = $valueTags['label'];
		$tagsObject[] = $tagObject;
	}
	return $tagsObject;
}

function InjectDataIntoView($userEmail, $tags) {
	$templateManager = new templateManager();
	$templateManager->render("admin-show",
		[
			'userEmail' => $userEmail,
			'tags' => $tags
		]);
}

function UploadShowImage() {
	$target_dir = "img/";

	if (isset($_FILES['fileToUpload']['name'])) {
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadResult = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
		return $uploadResult;
	}
	return false;
}

function AddShow() {
	$img_folder = "img/";
	$show = new Show();
	$showDao = new ShowSqlDao();

	if (isset($_FILES["fileToUpload"], $_POST["title"],
		$_POST["description"], $_POST["nb_season"])) {
		$show->img_url = $img_folder . basename($_FILES['fileToUpload']['name']);
		$show->title = $_POST["title"];
		$show->description = $_POST["description"];
		$show->nb_season = $_POST["nb_season"];

		$showResult = $showDao->addShow($show);
		echo $showResult;
	}
}

function AddShowTags() {
	if (isset($_POST["tags"])) {
		$showTagsObject;
		foreach ($_POST["tags"] as $keyTags => $valueTags) {
			$showTag = new ShowTag();
			$showTag->tag_id = $valueTags;
			$showTagsObject[] = $showTag;
		}
		$showTagDao = new ShowTagSqlDao();
		$showTagResult = $showTagDao->addShowTags($showTagsObject);
	}
}

?>

