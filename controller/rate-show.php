<?php

$isFromAjax = isset($_SERVER['HTTP_FROM_AJAX']);

if (!isset($_SESSION["user"])) {
	header('Location: index.php?ctrl=login');
	exit;
}
$currentUser = $_SESSION["user"];

$show = new Show();
$show->id = $_GET['showId'];

if (isset($_GET['rate'], $show, $currentUser)) {
	$dao = new RateShowSqlDao();
	$dao->updateRate($_GET['rate'], $show, $currentUser);
}

if ($isFromAjax) {
	echo 'OK';
}
else {
	header('Location: index.php?ctrl=list-show');
	exit;
}


?>