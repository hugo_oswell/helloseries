<?php

if (!isset($_SESSION["user"])) {
	header('Location: index.php?ctrl=login');
	exit;
}
$currentUser = $_SESSION["user"];

$dao = new ShowSqlDao();
$page = isset($_GET['page']) ? $_GET['page'] : 0;
$nbTotalResult = $dao->countAll();
$pagination;

if (isset($nbTotalResult)) {
	$pagination = new Pagination();
	$pagination->nbPerPage = 3;
	$pagination->currentPage = $page;
	$pagination->nbTotal = $nbTotalResult;
}
$nbPage = $pagination->getNbPage();

$action;
$list = [];

if (isset($_REQUEST['action'])) {
	$action = $_REQUEST['action'];
}


if (isset($action))
switch ($action) {
	case 'all':
		$list = $dao->listAll($pagination);
		break;
	case 'marked':
		$list = $dao->listMarked($currentUser);
		break;
	case 'reco':
		$list = $dao->listRecommendations($currentUser);
		break;
	default :
		$list = $dao->listAll($currentUser);
		break;
}


$templateManager = new templateManager();
$templateManager->render("list-show",
	[
		'shows' => $list,
		'pagination' => $pagination,
		'nbPage' => $nbPage
	]);

?>