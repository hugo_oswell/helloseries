<?php

class Pagination
{
	public $nbPerPage;
	public $currentPage;
	public $nbTotal;

	public function getNbPage() {
		return ceil($this->nbTotal / $this->nbPerPage);
	}

	public function getOffset() {
		return $this->currentPage * $this->nbPerPage;
	}
}

?>