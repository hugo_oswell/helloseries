<?php

require_once("entity/user.php");
require_once("entity/tag.php");
require_once("entity/showTag.php");
require_once("entity/show.php");
require_once("entity/pagination.php");
require_once("dao/userSqlDao.php");
require_once("dao/showSqlDao.php");
require_once("dao/tagSqlDao.php");
require_once("dao/showTagSqlDao.php");
require_once("dao/rateShowSqlDao.php");
require_once("template/templateManager.php");

session_start();

$controller = 'login';

if (isset($_REQUEST['ctrl'])) {
	$controller = $_REQUEST['ctrl'];

	if (!file_exists("controller/$controller.php")) {
		$controller = '404';
	}
}

include("controller/$controller.php");

?>