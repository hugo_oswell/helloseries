<?php
require_once("abstractSqlDao.php");

class RateShowSqlDao extends AbstractSqlDao
{
	public function updateRate($rate, Show $show, User $user)
	{
		$request = $this->pdo->prepare("REPLACE `show_user_note`(show_id, user_id, note) VALUES (:show_id, :user_id, :rate)");

		$request->bindParam(':show_id', $show->id);
		$request->bindParam(':user_id', $user->id);
		$request->bindParam(':rate', $rate);

		$result = $request->execute();
		return (!$result ? 'Error updating rate' : 'The rate has been registered.');
	}
}

?>