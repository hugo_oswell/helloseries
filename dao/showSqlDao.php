<?php

require_once("abstractSqlDao.php");

class ShowSqlDao extends AbstractSqlDao
{
	public function getShow(int $show_id) {
		$request = $this->pdo->prepare("SELECT title, description, image_url, nb_season FROM `Show` WHERE id=:show_id");
		$request->bindParam(":show_id", $show_id);
		$request->execute();

		$result = $request->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function addShow(Show $show) {
		$request = $this->pdo->prepare("INSERT INTO `Show`(title, description, image_url, nb_season) VALUES (:title, :description, :image_url, :nb_season)");

		$request->bindParam(':title', $show->title);
		$request->bindParam(':description', $show->description);
		$request->bindParam(':image_url', $show->img_url);
		$request->bindParam(':nb_season', $show->nb_season);

		$result = $request->execute();

		return (!$result ? 'Error inserting show' : 'OK');
	}

	public function countAll()
	{
		$request = $this->pdo->prepare("SELECT COUNT(id) as 'nbResult' FROM `Show`");
		$request->execute();
		$result = $request->fetch(PDO::FETCH_ASSOC);
		return $result['nbResult'];
	}

	public function listAll(Pagination $pagination) {
		$request = "SELECT * FROM `Show` ORDER BY Id DESC";
		$requestParameters = null;
		return $this->listShows($request, $requestParameters, $pagination);
	}

	public function listMarked(User $user) {
		$request = "SELECT `show`.id, `show`.title, `show`.description,
			`show`.image_url, `show`.nb_season, show_user_note.note
			FROM show_user_note JOIN `show` ON `show`.id = show_user_note.show_id 
			WHERE user_id = :user_id
			ORDER BY show_user_note.note DESC";
		$requestParameters = 
		[
			':user_id' => $user->id
		];
		return $this->listShows($request, $requestParameters);
	}

	public function listRecommendations(User $user) {
		$request = "SELECT `show`.id, `show`.title, `show`.description,
			`show`.image_url, `show`.nb_season,
			sum(notation_tag.notation_tag_note) as show_note
			from `show`
			left join show_tag  on show_tag.show_id = `show`.id 
			left join show_user_note on show_user_note.show_id = `show`.id and  show_user_note.user_id = :user_id
			left join (
			        select
			        tag.id as notation_tag_id,
			        sum(show_user_note.note) as notation_tag_note
			        from show_user_note
			        left join `show` on `show`.id = show_user_note.show_id
			        left join show_tag on show_tag.show_id = `show`.id
			        left join tag on tag.id = show_tag.tag_id
			        where user_id = :user_id
			        group by tag.id
			        order by notation_tag_note DESC
			) as notation_tag on show_tag.tag_id = notation_tag.notation_tag_id
			where show_user_note.show_id is  null
			group by `show`.id
			order by show_note DESC";
		$requestParameters = 
		[
			':user_id' => $user->id
		];
		return $this->listShows($request, $requestParameters);
	}

	public function listShows($request, $requestParameters, Pagination $pagination) {
		$offset = $pagination->getOffset();
		$request .= " LIMIT $offset, $pagination->nbPerPage";
		$request = $this->pdo->prepare($request);
		if (!is_null($requestParameters)) {
			foreach ($requestParameters as $key => $value) {
				$request->bindParam($key, $value);
			}
		}
		$result = $request->execute();

		$shows = [];

		if (!$result) {
			return $shows;
		}

		while ($line = $request->fetch(PDO::FETCH_ASSOC)) {
			$show = new Show();
			$show->id = $line['id'];
			$show->title = $line['title'];
			$show->description = $line['description'];
			$show->img_url = $line['image_url'];
			$show->nb_season = $line['nb_season'];
			if (isset($line['show_note'])) {
				$show->mark = $line['show_note'];
			}
			if (isset($line['note'])) {
				$show->mark = $line['note'];
			}
			$shows[] = $show;
		}

		return $shows;
	}
}

?>