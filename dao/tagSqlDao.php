<?php

require_once("abstractSqlDao.php");

class TagSqlDao extends AbstractSqlDao
{
	public function getTags()
	{
		$tags;
		$request = $this->pdo->prepare("SELECT * FROM Tag");
		$request->execute();
		while ($tag = $request->fetch(PDO::FETCH_ASSOC)) {
  			$tags[] = $tag;
		}
		return $tags;
	}
}

?>