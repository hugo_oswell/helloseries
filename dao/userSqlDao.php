<?php

require_once("abstractSqlDao.php");

class UserSqlDao extends AbstractSqlDao
{
	public function checkLogin(User $user)
	{
		$request = $this->pdo->prepare("SELECT id FROM User WHERE email=:email AND password=:password");
		$request->bindParam(":email", $user->email);
		$request->bindParam(":password", $user->password);
		$request->execute();

		$result = $request->fetch(PDO::FETCH_ASSOC);

		if (!$result) {
			return false;
		}
		$user->id = $result["id"];
		$user->password = null;
		return true;
	}
}

?>