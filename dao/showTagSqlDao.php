<?php

require_once("abstractSqlDao.php");

class ShowTagSqlDao extends AbstractSqlDao
{
	public function addShowTags($showTags) {
		//$lastShowInsertedId = $this->pdo->lastInsertId();
		$lastShowInsertedId = $request = $this->pdo->prepare("SELECT MAX(id) as 'max' FROM `Show`");
		$request->execute();
		$result = $request->fetch(PDO::FETCH_ASSOC);

		foreach ($showTags as $key => $value) {
			$value->show_id = $result['max'];
			$this->addShowTag($value);
		}
	}

	public function addShowTag(ShowTag $showTag) {
		$request = $this->pdo->prepare("INSERT INTO `show_tag`(show_id, tag_id) VALUES (:show_id, :tag_id)");

		$request->bindParam(':show_id', $showTag->show_id);
		$request->bindParam(':tag_id', $showTag->tag_id);

		$result = $request->execute();

		return (!$result ? 'Error inserting showTag' : 'The Show tag has been registered.');
	}
}

?>