document.addEventListener("DOMContentLoaded", function() {
	console.log("DOMContentLoaded");

	var nodes = document.querySelectorAll(".markComponent a");

	for (var i = 0; i < nodes.length; i++) {
		var node = nodes[i];
		node.addEventListener("click", function(event) {
			event.preventDefault();
			var url = event.target.getAttribute("href");
			
			var request = new XMLHttpRequest();
			//Prépare la requête
			request.open('GET', url);
			//Clé - valeur où l'on met ce que l'on veut
			request.setRequestHeader("from-ajax", "true");
			//Ne lance rien, on prépare simplement un écouteur
			request.addEventListener("load", function() {
				//Masquer le loader
				event.target.parentNode.classList.remove("waiting");
				var previous = event.target.parentNode.querySelector("a.highlight");
				//Remove previous mark if exists
				if (previous) {
					previous.classList.remove("highlight");
				}
				//Highlight new mark
				event.target.classList.add("highlight");
			});
			//Envoie la requête
			request.send();
			//Afficher le loader
			event.target.parentNode.classList.add("waiting");
		});
	}
});