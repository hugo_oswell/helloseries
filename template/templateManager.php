<?php

class templateManager
{
	public function render($templateName, $data)
	{
		extract($data);
		include("view/$templateName.view.php");
	}
}

?>