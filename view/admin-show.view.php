<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div><?= $userEmail ?></div>
		<h1>Show editing</h1>
		<form method="post" action="index.php?ctrl=admin-show" enctype="multipart/form-data">
			<div>
				<label for="title">Titre :</label>
				<input id="title" type="text" name="title">
			</div>
			<div>
				<label for="description">Description :</label>
				<input id="description" type="text" name="description">
			</div>
			<div>
				<label for="tags">Tags : </label>
				<ul>
				<?php
					foreach ($tags as $keyTags => $valueTags) {
						echo '<li>';
						echo '<label>' . $valueTags->label . '</label>';
						echo '<input type="checkbox" name="tags[]" value="' . $valueTags->id . '">';
						echo '</li>';
					}
				?>
				</ul>
			</div>
			<div>
				<label for="nb_season">Nombre de saisons :</label>
				<input id="nb_season" type="text" name="nb_season">
			</div>
			<div>
				<label for="fileToUpload">Image : </label>
				<input type="file" name="fileToUpload" id="fileToUpload">
			</div>
			<div>
				<button type="submit">Add the show</button>
			</div>
		</form>
	</body>
</html>