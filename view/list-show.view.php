<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../css/style.css">
	</head>
	<body>
		<h1>Hello Series</h1>
		<form action="index.php?ctrl=list-show" method="post">
			<select name="action" id="action">
				<option value="all">All</option>
				<option value="marked">Marked</option>
				<option value="reco">Recommendations</option>
			</select>
			<input type="submit" value="Search">
		</form>
		<section>
			<?php
				foreach ($shows as $show) { ?>
				 	<article>
				 		<?php if ($show->mark) { ?>
				 			<div>Mark : <?= $show->mark; ?></div>
						<?php } ?>
				 		<div>Title : <?= $show->title; ?></div>
				 		<div>Description : <?= $show->description; ?></div>
				 		<div><?= $show->nb_season; ?> Season(s)</div>

				 	</article>
				 	<div class="markComponent">
				 		<br>
				 		<span>Rate : </span>
				 		<?php
				 			$maxRate = 5;
				 			for ($i = 1; $i <= $maxRate ; $i++) {
				 				if ($i == $show->mark) {
				 					echo '<a href="index.php?ctrl=rate-show&rate='.$i.'&showId='.$show->id.'" class="rate highlight"> '.$i.' </a>';
				 				}
				 				else {
				 					echo '<a href="index.php?ctrl=rate-show&rate='.$i.'&showId='.$show->id.'" class="rate"> '.$i.' </a>';
				 				}
				 			}
				 		?>
				 	</div>
				 	<br>
			<?php } ?>
		</section>
		<?php
			echo '<div class="center">';
			echo '<span>Pages : </span>';
			for ($i = 0; $i < $nbPage; $i++) { 
				echo '<a href="index.php?ctrl=list-show&action=all&page='.($i+1).'"> '.($i+1).'</a>';
			}
			echo '</div>';
		?>
		<script type="text/javascript" src="js/module-rate.js"></script>
	</body>
</html>